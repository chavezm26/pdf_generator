import 'dart:core';

import 'package:floor/floor.dart';

@Entity(tableName: 'shift_records', primaryKeys: ['id'])
class ShiftRecordModel {
  String id;
  int expectedStartTime;
  int expectedEndTime;
  int actualStartTime;
  int actualEndTime;

  ShiftRecordModel({
    required this.id,
    required this.expectedStartTime,
    required this.expectedEndTime,
    required this.actualStartTime,
    required this.actualEndTime,
  });

  ShiftRecordModel.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        expectedStartTime = json['expectedStartTime'],
        expectedEndTime = json['expectedEndTime'],
        actualStartTime = json['actualStartTime'],
        actualEndTime = json['actualEndTime'];

  Map<String, dynamic> toJson() => {
        "id": id,
        "expectedStartTime": expectedStartTime,
        "expectedEndTime": expectedEndTime,
        "actualStartTime": actualStartTime,
        "actualEndTime": actualEndTime,
      };

  @override
  String toString() {
    String result =
        "expectedStartTime: $expectedStartTime \nexpectedEndTime: $expectedEndTime \nactualStartTime: $actualStartTime \nactualEndTime: $actualEndTime";
    return result;
  }
}
