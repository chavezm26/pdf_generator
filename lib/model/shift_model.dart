import 'dart:core';

import 'package:floor/floor.dart';

@Entity(tableName: 'shifts', primaryKeys: ['id'])
class ShiftModel {
  String id;
  String imei;
  String name;
  int version;

  ShiftModel({
    required this.id,
    required this.imei,
    required this.name,
    required this.version,
  });

  ShiftModel.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        imei = json['imei'],
        name = json['name'],
        version = json['version'];

  Map<String, dynamic> toJson() => {
        "id": id,
        "imei": imei,
        "name": name,
        "version": version,
      };

  @override
  String toString() {
    String result =
        "\nid: $id \nshiftId: $imei \nname: $name  \nversion: $version";
    return result;
  }
}
