import 'dart:core';

class PdfHeaderModel {
  String reportName;
  String companyName;
  String? businessUnit;
  String deviceImei;
  int shiftStart;
  int shiftEnd;
  int shiftEdited;
  int minTime;

  PdfHeaderModel({
    required this.reportName,
    required this.companyName,
    required this.businessUnit,
    required this.deviceImei,
    required this.shiftStart,
    required this.shiftEnd,
    required this.shiftEdited,
    required this.minTime,
  });

  PdfHeaderModel.fromJson(Map<String, dynamic> json)
      : reportName = json['reportName'],
        companyName = json['companyName'],
        businessUnit = json['businessUnit'],
        deviceImei = json['deviceImei'],
        shiftStart = json['shiftStart'],
        shiftEnd = json['shiftEnd'],
        shiftEdited = json['shiftEdited'],
        minTime = json['minTime'];

  Map<String, dynamic> toJson() => {
        "reportName": reportName,
        "companyName": companyName,
        "businessUnit": businessUnit,
        "deviceImei": deviceImei,
        "shiftStart": shiftStart,
        "shiftEnd": shiftEnd,
        "shiftEdited": shiftEdited,
        "minTime": minTime,
      };

  @override
  String toString() {
    String result =
        "reportName: $reportName \ncompanyName: $companyName\nbusinessUnit: $businessUnit\ndeviceImei: $deviceImei\nshiftStart: $shiftStart\nshiftEnd: $shiftEnd\nshiftEdited: $shiftEdited\nminTime: $minTime";
    return result;
  }
}
