import 'dart:core';

import 'package:floor/floor.dart';

@Entity(tableName: 'point_scans', primaryKeys: ['id'])
class PointsModel {
  int id;
  String tagName;
  int timestamp;

  PointsModel({
    required this.id,
    required this.tagName,
    required this.timestamp,
  });

  PointsModel.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        tagName = json['tagName'],
        timestamp = json['timestamp'];

  Map<String, dynamic> toJson() => {
        "id": id,
        "tagName": tagName,
        "timestamp": timestamp,
      };

  @override
  String toString() {
    String result = "tagName: $tagName \ntimestamp: $timestamp";
    return result;
  }
}
