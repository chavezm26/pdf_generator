import 'dart:core';

import 'package:floor/floor.dart';

@Entity(tableName: 'patrols', primaryKeys: ['id'])
class PatrolModel {
  String id;
  String shiftId;
  String siteId;
  String companyId;
  String? businessUnitId;
  String siteName;
  int day;
  String type;
  int startTime;
  int endTime;
  int? actualStartTime;
  int? actualEndTime;
  int? totalPoints;
  int? scannedPoints;

  PatrolModel({
    required this.id,
    required this.shiftId,
    required this.siteId,
    required this.companyId,
    required this.businessUnitId,
    required this.siteName,
    required this.day,
    required this.type,
    required this.startTime,
    required this.endTime,
    required this.actualStartTime,
    required this.actualEndTime,
    required this.totalPoints,
    required this.scannedPoints,
  });

  PatrolModel.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        shiftId = json['shiftId'],
        siteId = json['siteId'],
        companyId = json['companyId'],
        businessUnitId = json['businessUnitId'],
        siteName = json['siteName'],
        day = json['day'],
        type = json['type'],
        startTime = json['startTime'],
        endTime = json['endTime'],
        actualStartTime = json['actualStartTime'],
        actualEndTime = json['actualEndTime'],
        totalPoints = json['totalPoints'],
        scannedPoints = json['scannedPoints'];

  Map<String, dynamic> toJson() => {
        "id": id,
        "shiftId": shiftId,
        "siteId": siteId,
        "companyId": companyId,
        "businessUnitId": businessUnitId,
        "siteName": siteName,
        "day": day,
        "type": type,
        "startTime": startTime,
        "endTime": endTime,
        "actualStartTime": actualStartTime,
        "actualEndTime": actualEndTime,
        "totalPoints": totalPoints,
        "scannedPoints": scannedPoints
      };

  @override
  String toString() {
    String result =
        "\nid: $id \nshiftId: $shiftId \nsiteId: $siteId \ncompanyId: $companyId \nbusinessUnitId: $businessUnitId \nsiteName: $siteName \nday: $day \ntype: $type \nstartTime: $startTime \nendTime: $endTime \nactualStartTime: $actualStartTime \nactualEndTime: $actualEndTime \ntotalPoints: $totalPoints \nscannedPoints: $scannedPoints";
    return result;
  }
}
