import 'package:flutter/material.dart';
import 'package:pdf_generator/services/pdf_api.dart';
import 'package:pdf_generator/services/pdf_invoice_api.dart';
import 'package:pdf_generator/services/pdf_service.dart';
import 'package:pdf_generator/sql/app_database.dart';
import 'package:pdf_generator/sql/dao/dao.dart';

import 'injection/injection.dart';
import 'model/patrol_model.dart';
import 'model/pdf_model.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  configureDependencies();
  final database = await $FloorAppDatabase
      .databaseBuilder('new_guarding_database_za')
      .build();
  final patrolDao = database.patrolDao;
  runApp(MainApp(dao: patrolDao));
}

class MainApp extends StatelessWidget {
  final PatrolDao dao;

  MainApp({super.key, required this.dao});
  final PdfService pdfService = PdfService();
  final PdfInvoiceApi _pdfInvoiceApi = PdfInvoiceApi();
  @override
  Widget build(BuildContext context) {
    String shiftId = "pwuWTyxWHAMSFlR2RYIp_1646191309027_1646414100000";
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: Column(children: <Widget>[
            const SizedBox(height: 20),
            ElevatedButton(
                style: TextButton.styleFrom(
                    foregroundColor: Colors.white,
                    backgroundColor: Colors.black),
                onPressed: () async {
                  //fetches shifts
                  var shiftRecords = await dao.fetchShiftRecords(shiftId);
                  print(shiftRecords);

                  //fetches all patrols
                  var patrols = await dao.fetchPatrolsById(shiftId);
                  //fetches patrolID
                  var patrolsId = patrols[1].toJson()["id"];

                  var patrolDetails = patrols[1].toJson();

                  print(patrolDetails);
                  //fetches imei/shiftName
                  var shiftDetails =
                      await dao.fetchShiftHeader(patrolDetails["shiftId"]);
                  print(shiftDetails);
                  var shiftJson = shiftDetails[0].toJson();
                  print(shiftJson);
                  //fetches points for a single patrol
                  var points = await dao.fetchPatrolPoints(patrolsId);
                  print(points);
                  var model = await pdfService.setModel(
                      reportName: shiftJson["name"],
                      companyName: "Large Security",
                      businessUnit: "",
                      deviceImei: shiftJson["imei"],
                      shiftStart: 0,
                      shiftEnd: 0,
                      shiftEdited: shiftJson["version"],
                      minTime: 0);

                  final pdfFile = await _pdfInvoiceApi.generate();
                  PdfApi.openFile(pdfFile);
                },
                child: const Text("Export to PDF"))
          ]),
        ),
      ),
    );
  }
}
