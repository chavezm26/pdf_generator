// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_database.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

// ignore: avoid_classes_with_only_static_members
class $FloorAppDatabase {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder databaseBuilder(String name) =>
      _$AppDatabaseBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$AppDatabaseBuilder(null);
}

class _$AppDatabaseBuilder {
  _$AppDatabaseBuilder(this.name);

  final String? name;

  final List<Migration> _migrations = [];

  Callback? _callback;

  /// Adds migrations to the builder.
  _$AppDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$AppDatabaseBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<AppDatabase> build() async {
    final path = name != null
        ? await sqfliteDatabaseFactory.getDatabasePath(name!)
        : ':memory:';
    final database = _$AppDatabase();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$AppDatabase extends AppDatabase {
  _$AppDatabase([StreamController<String>? listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  PatrolDao? _patrolDaoInstance;

  Future<sqflite.Database> open(
    String path,
    List<Migration> migrations, [
    Callback? callback,
  ]) async {
    final databaseOptions = sqflite.OpenDatabaseOptions(
      version: 1,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
        await callback?.onConfigure?.call(database);
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `patrols` (`id` TEXT NOT NULL, `shiftId` TEXT NOT NULL, `siteId` TEXT NOT NULL, `companyId` TEXT NOT NULL, `businessUnitId` TEXT, `siteName` TEXT NOT NULL, `day` INTEGER NOT NULL, `type` TEXT NOT NULL, `startTime` INTEGER NOT NULL, `endTime` INTEGER NOT NULL, `actualStartTime` INTEGER, `actualEndTime` INTEGER, `totalPoints` INTEGER, `scannedPoints` INTEGER, PRIMARY KEY (`id`))');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `point_scans` (`id` INTEGER NOT NULL, `tagName` TEXT NOT NULL, `timestamp` INTEGER NOT NULL, PRIMARY KEY (`id`))');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `shift_records` (`id` TEXT NOT NULL, `expectedStartTime` INTEGER NOT NULL, `expectedEndTime` INTEGER NOT NULL, `actualStartTime` INTEGER NOT NULL, `actualEndTime` INTEGER NOT NULL, PRIMARY KEY (`id`))');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `shifts` (`id` TEXT NOT NULL, `imei` TEXT NOT NULL, `name` TEXT NOT NULL, `version` INTEGER NOT NULL, PRIMARY KEY (`id`))');

        await callback?.onCreate?.call(database, version);
      },
    );
    return sqfliteDatabaseFactory.openDatabase(path, options: databaseOptions);
  }

  @override
  PatrolDao get patrolDao {
    return _patrolDaoInstance ??= _$PatrolDao(database, changeListener);
  }
}

class _$PatrolDao extends PatrolDao {
  _$PatrolDao(
    this.database,
    this.changeListener,
  ) : _queryAdapter = QueryAdapter(database);

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  @override
  Future<List<PatrolModel>> fetchAllPatrols() async {
    return _queryAdapter.queryList('SELECT * FROM patrols',
        mapper: (Map<String, Object?> row) => PatrolModel(
            id: row['id'] as String,
            shiftId: row['shiftId'] as String,
            siteId: row['siteId'] as String,
            companyId: row['companyId'] as String,
            businessUnitId: row['businessUnitId'] as String?,
            siteName: row['siteName'] as String,
            day: row['day'] as int,
            type: row['type'] as String,
            startTime: row['startTime'] as int,
            endTime: row['endTime'] as int,
            actualStartTime: row['actualStartTime'] as int?,
            actualEndTime: row['actualEndTime'] as int?,
            totalPoints: row['totalPoints'] as int?,
            scannedPoints: row['scannedPoints'] as int?));
  }

  @override
  Stream<List<String>> getSiteName() {
    return _queryAdapter.queryListStream('SELECT siteName FROM patrols',
        mapper: (Map<String, Object?> row) => row.values.first as String,
        queryableName: 'patrols',
        isView: false);
  }

  @override
  Future<List<PatrolModel>> fetchPatrolsById(String id) async {
    return _queryAdapter.queryList('SELECT * FROM patrols WHERE recordId = ?1',
        mapper: (Map<String, Object?> row) => PatrolModel(
            id: row['id'] as String,
            shiftId: row['shiftId'] as String,
            siteId: row['siteId'] as String,
            companyId: row['companyId'] as String,
            businessUnitId: row['businessUnitId'] as String?,
            siteName: row['siteName'] as String,
            day: row['day'] as int,
            type: row['type'] as String,
            startTime: row['startTime'] as int,
            endTime: row['endTime'] as int,
            actualStartTime: row['actualStartTime'] as int?,
            actualEndTime: row['actualEndTime'] as int?,
            totalPoints: row['totalPoints'] as int?,
            scannedPoints: row['scannedPoints'] as int?),
        arguments: [id]);
  }

  @override
  Future<List<PointsModel>> fetchPatrolPoints(String id) async {
    return _queryAdapter.queryList(
        'SELECT * FROM point_scans WHERE patrolId = ?1',
        mapper: (Map<String, Object?> row) => PointsModel(
            id: row['id'] as int,
            tagName: row['tagName'] as String,
            timestamp: row['timestamp'] as int),
        arguments: [id]);
  }

  @override
  Future<List<ShiftModel>> fetchShiftHeader(String id) async {
    return _queryAdapter.queryList('SELECT * FROM shifts WHERE id = ?1',
        mapper: (Map<String, Object?> row) => ShiftModel(
            id: row['id'] as String,
            imei: row['imei'] as String,
            name: row['name'] as String,
            version: row['version'] as int),
        arguments: [id]);
  }

  @override
  Future<List<ShiftRecordModel>> fetchShiftRecords(String id) async {
    return _queryAdapter.queryList('SELECT * FROM shift_records WHERE id = ?1',
        mapper: (Map<String, Object?> row) => ShiftRecordModel(
            id: row['id'] as String,
            expectedStartTime: row['expectedStartTime'] as int,
            expectedEndTime: row['expectedEndTime'] as int,
            actualStartTime: row['actualStartTime'] as int,
            actualEndTime: row['actualEndTime'] as int),
        arguments: [id]);
  }
}
