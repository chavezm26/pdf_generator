import 'dart:async';
import 'package:floor/floor.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

import '../model/patrol_model.dart';
import '../model/points_scan.dart';
import '../model/shift_model.dart';
import '../model/shift_record_model.dart';
import 'dao/dao.dart';

part 'app_database.g.dart'; // the generated code will be there

@Database(
    version: 1,
    entities: [PatrolModel, PointsModel, ShiftRecordModel, ShiftModel])
abstract class AppDatabase extends FloorDatabase {
  PatrolDao get patrolDao;
}
