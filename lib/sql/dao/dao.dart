import 'package:floor/floor.dart';

import '../../model/patrol_model.dart';
import '../../model/points_scan.dart';
import '../../model/shift_model.dart';
import '../../model/shift_record_model.dart';

@dao
abstract class PatrolDao {
  @Query('SELECT * FROM patrols')
  Future<List<PatrolModel>> fetchAllPatrols();

  @Query('SELECT siteName FROM patrols')
  Stream<List<String>> getSiteName();

  @Query('SELECT * FROM patrols WHERE recordId = :id')
  Future<List<PatrolModel>> fetchPatrolsById(String id);

  @Query('SELECT * FROM point_scans WHERE patrolId = :id')
  Future<List<PointsModel>> fetchPatrolPoints(String id);

  @Query('SELECT * FROM shifts WHERE id = :id')
  Future<List<ShiftModel>> fetchShiftHeader(String id);

  @Query('SELECT * FROM shift_records WHERE id = :id')
  Future<List<ShiftRecordModel>> fetchShiftRecords(String id);
}
