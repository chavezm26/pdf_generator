import 'dart:math';

import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart';
import 'package:pdf_generator/model/pdf_model.dart';
import 'package:pdf_generator/services/pdf_api.dart';
import 'package:pdf_generator/services/pdf_service.dart';
import 'package:pdf_generator/sql/app_database.dart';

import '../injection/injection.dart';

class PdfInvoiceApi {
  generate() async {
    final pdf = Document();
    final date = DateTime.now();
    String dateNow = DateFormat("yyyy-MM-dd HH:mm").format(date);
    final font = await rootBundle.load("assets/OpenSans-Regular.ttf");
    final ttf = Font.ttf(font);
    PdfHeaderModel model = getIt<PdfService>().pdfModel;
    print(model);
    String report = model.reportName;
    String passRate = "97";
    String companyName = model.companyName;
    String device = model.deviceImei;
    String editted = model.shiftEdited.toString();
    String start = model.shiftStart.toString();
    String end = model.shiftEnd.toString();
    int patrolNo = 0;
    const dataTable = [
      [
        'Complete',
        8,
      ],
      [
        'Incomplete',
        25,
      ],
      [
        'Failed',
        30,
      ],
    ];

    const dataTable2 = [
      ['Scanned', 90, 10],
      ['Missed', 10, 90],
    ];

    final chart1 = Chart(
      left: Container(
        alignment: Alignment.topCenter,
        margin: const EdgeInsets.only(right: 5, top: 10),
        child: Transform.rotateBox(
          angle: pi / 2,
        ),
      ),
      grid: CartesianGrid(
        xAxis: FixedAxis.fromStrings(
          List<String>.generate(
              dataTable.length, (index) => dataTable[index][0] as String),
          marginStart: 30,
          marginEnd: 30,
          ticks: true,
        ),
        yAxis: FixedAxis(
          [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
          format: (v) => '$v',
          divisions: true,
        ),
      ),
      datasets: [
        BarDataSet(
          color: PdfColors.green,
          width: 100,
          offset: -10,
          borderColor: PdfColors.green,
          data: List<PointChartValue>.generate(
            dataTable.length,
            (i) {
              final v = dataTable[i][1] as num;
              return PointChartValue(i.toDouble(), v.toDouble());
            },
          ),
        ),
      ],
    );

    final chart2 = Chart(
      left: Container(
        alignment: Alignment.topCenter,
        margin: const EdgeInsets.only(right: 5, top: 10),
        child: Transform.rotateBox(
          angle: pi / 2,
        ),
      ),
      grid: CartesianGrid(
        xAxis: FixedAxis.fromStrings(
          List<String>.generate(
              dataTable2.length, (index) => dataTable2[index][0] as String),
          marginStart: 30,
          marginEnd: 30,
          ticks: true,
        ),
        yAxis: FixedAxis(
          [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
          format: (v) => '$v',
          divisions: true,
        ),
      ),
      datasets: [
        BarDataSet(
          color: PdfColors.yellow,
          width: 100,
          offset: -10,
          borderColor: PdfColors.yellow,
          data: List<PointChartValue>.generate(
            dataTable2.length,
            (i) {
              final v = dataTable2[i][1] as num;
              return PointChartValue(i.toDouble(), v.toDouble());
            },
          ),
        ),
      ],
    );

    pdf.addPage(
      MultiPage(
        pageFormat: PdfPageFormat.a4,
        margin: const EdgeInsets.all(32),
        build: (Context context) {
          return [
            Header(
              padding: const EdgeInsets.all(5),
              decoration: BoxDecoration(
                  color: const PdfColor.fromInt(0xffe0e0e0),
                  border:
                      Border.all(color: const PdfColor.fromInt(0xffbdbdbd))),
              margin: const EdgeInsets.all(30),
              level: 0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                          child: Text(
                        'Report: $report',
                        style: TextStyle(font: ttf, fontSize: 11),
                      )),
                      Text('Pass Rate: $passRate%',
                          style: TextStyle(font: ttf, fontSize: 11)),
                    ],
                  ),
                  Divider(color: const PdfColor.fromInt(0xffbdbdbd)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Company Name: $companyName",
                          style: TextStyle(fontSize: 10, fontBold: ttf)),
                      Text("Business Unit: ",
                          style: TextStyle(fontSize: 10, fontBold: ttf)),
                    ],
                  ),
                  SizedBox(height: 5),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Report Created: $dateNow",
                          style: TextStyle(fontSize: 10, fontBold: ttf)),
                      Text("Qualified Report",
                          style: TextStyle(
                              fontSize: 10,
                              fontBold: ttf,
                              color: const PdfColor.fromInt(0xff4caf50))),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Device: $device",
                          style: TextStyle(fontSize: 10, fontBold: ttf)),
                      Text("Shift Edited: $editted",
                          style: TextStyle(fontSize: 10, fontBold: ttf)),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Shift Start: $start",
                          style: TextStyle(fontSize: 10, fontBold: ttf)),
                      Text("Shift End: $end",
                          style: TextStyle(fontSize: 10, fontBold: ttf)),
                    ],
                  ),
                  Text("Min Time between points: ",
                      style: TextStyle(fontSize: 10, fontBold: ttf)),
                ],
              ),
            ),
            SizedBox(height: 5),
            Header(
              padding: const EdgeInsets.all(5),
              decoration: BoxDecoration(
                  color: const PdfColor.fromInt(0xffe0e0e0),
                  border:
                      Border.all(color: const PdfColor.fromInt(0xffbdbdbd))),
              margin: const EdgeInsets.only(left: 30, right: 30),
              level: 0,
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        'Patrol Peformance ',
                        style: TextStyle(
                            font: ttf,
                            fontSize: 11,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 30),
                  child: Container(
                    height: 200,
                    width: 150,
                    alignment: Alignment.topLeft,
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: const PdfColor.fromInt(0xffbdbdbd))),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Text("No. of Patrols: "),
                          Text("Passed Patrols: "),
                          Text("Failed Patrols:  "),
                          Text("Incomplete: "),
                        ]),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 30),
                  child: SizedBox(
                    height: 200,
                    width: 325,
                    child: Expanded(flex: 3, child: chart1),
                  ),
                ),
              ],
            ),
            Header(
              padding: const EdgeInsets.all(5),
              decoration: BoxDecoration(
                  color: const PdfColor.fromInt(0xffe0e0e0),
                  border:
                      Border.all(color: const PdfColor.fromInt(0xffbdbdbd))),
              margin: const EdgeInsets.only(left: 30, right: 30, top: 20),
              level: 0,
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        'Tag Peformance ',
                        style: TextStyle(
                            font: ttf,
                            fontSize: 11,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 30),
                  child: Container(
                    height: 200,
                    width: 150,
                    alignment: Alignment.topLeft,
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: const PdfColor.fromInt(0xffbdbdbd))),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Text("Total Points: "),
                          Text("Scanned Points: "),
                          Text("Missed Points:  "),
                        ]),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 30),
                  child: SizedBox(
                    height: 200,
                    width: 325,
                    child: Expanded(flex: 3, child: chart2),
                  ),
                ),
              ],
            ),
            Header(
              padding: const EdgeInsets.all(5),
              decoration: BoxDecoration(
                  color: const PdfColor.fromInt(0xffe0e0e0),
                  border:
                      Border.all(color: const PdfColor.fromInt(0xffbdbdbd))),
              margin: const EdgeInsets.all(30),
              level: 0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                          child: Text(
                        'Controller Notifications ',
                        style: TextStyle(
                            font: ttf,
                            fontSize: 11,
                            fontWeight: FontWeight.bold),
                      )),
                    ],
                  ),
                  Divider(color: const PdfColor.fromInt(0xffbdbdbd)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Source ",
                          style: TextStyle(fontSize: 10, fontBold: ttf)),
                      Container(
                        color: const PdfColor.fromInt(0xffbdbdbd),
                        width: 1,
                        height: 15,
                      ),
                      Text("Time ",
                          style: TextStyle(fontSize: 10, fontBold: ttf)),
                      Container(
                        color: const PdfColor.fromInt(0xffbdbdbd),
                        width: 1,
                        height: 15,
                      ),
                      Text("Controller ",
                          style: TextStyle(fontSize: 10, fontBold: ttf)),
                      Container(
                        color: const PdfColor.fromInt(0xffbdbdbd),
                        width: 1,
                        height: 15,
                      ),
                      Text("Comment ",
                          style: TextStyle(fontSize: 10, fontBold: ttf)),
                    ],
                  ),
                ],
              ),
            ),
            Header(
              padding: const EdgeInsets.all(5),
              decoration: BoxDecoration(
                  color: const PdfColor.fromInt(0xffe0e0e0),
                  border:
                      Border.all(color: const PdfColor.fromInt(0xffbdbdbd))),
              margin: const EdgeInsets.only(left: 30, right: 30, top: 30),
              level: 0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                          child: Text(
                        'Battery Notifications ',
                        style: TextStyle(
                            font: ttf,
                            fontSize: 11,
                            fontWeight: FontWeight.bold),
                      )),
                    ],
                  ),
                  Divider(color: const PdfColor.fromInt(0xffbdbdbd)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text("Time ",
                          style: TextStyle(fontSize: 10, fontBold: ttf)),
                      Container(
                        color: const PdfColor.fromInt(0xffbdbdbd),
                        width: 1,
                        height: 15,
                      ),
                      Text("Battery Level ",
                          style: TextStyle(fontSize: 10, fontBold: ttf)),
                    ],
                  ),
                ],
              ),
            ),
            Header(
              padding: const EdgeInsets.all(5),
              decoration: BoxDecoration(
                  color: const PdfColor.fromInt(0xffe0e0e0),
                  border:
                      Border.all(color: const PdfColor.fromInt(0xffbdbdbd))),
              margin: const EdgeInsets.only(left: 30, right: 30, top: 20),
              level: 0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                          child: Text(
                        'Online/Offline History ',
                        style: TextStyle(
                            font: ttf,
                            fontSize: 11,
                            fontWeight: FontWeight.bold),
                      )),
                    ],
                  ),
                  Divider(color: const PdfColor.fromInt(0xffbdbdbd)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text("Time Online",
                          style: TextStyle(fontSize: 10, fontBold: ttf)),
                      Container(
                        color: const PdfColor.fromInt(0xffbdbdbd),
                        width: 1,
                        height: 15,
                      ),
                      Text("Offline ",
                          style: TextStyle(fontSize: 10, fontBold: ttf)),
                    ],
                  ),
                ],
              ),
            ),
            ListView.builder(
              itemCount: 5,
              itemBuilder: ((context, index) {
                patrolNo = index + 1;
                return Header(
                  padding: const EdgeInsets.all(5),
                  decoration: BoxDecoration(
                      color: const PdfColor.fromInt(0xffe0e0e0),
                      border: Border.all(
                          color: const PdfColor.fromInt(0xffbdbdbd))),
                  margin: const EdgeInsets.only(left: 30, right: 30, top: 20),
                  level: 0,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                              child: Text(
                            'Patrols ',
                            style: TextStyle(
                                font: ttf,
                                fontSize: 11,
                                fontWeight: FontWeight.bold),
                          )),
                        ],
                      ),
                      Divider(color: const PdfColor.fromInt(0xffbdbdbd)),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text("Patrol",
                              style: TextStyle(fontSize: 10, fontBold: ttf)),
                          Container(
                            color: const PdfColor.fromInt(0xffbdbdbd),
                            width: 1,
                            height: 10,
                          ),
                          Text("Tags ",
                              style: TextStyle(fontSize: 10, fontBold: ttf)),
                          Container(
                            color: const PdfColor.fromInt(0xffbdbdbd),
                            width: 1,
                            height: 10,
                          ),
                          Text("Completed ",
                              style: TextStyle(fontSize: 10, fontBold: ttf)),
                        ],
                      ),
                      Divider(color: const PdfColor.fromInt(0xffbdbdbd)),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Text("Time From:",
                              style: TextStyle(fontSize: 10, fontBold: ttf)),
                        ],
                      ),
                      Divider(color: const PdfColor.fromInt(0xffbdbdbd)),
                      Container(
                        decoration: BoxDecoration(
                            color: PdfColors.white,
                            border: Border.all(color: PdfColors.white)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Text(patrolNo.toString(),
                                style: TextStyle(fontSize: 10, fontBold: ttf)),
                            Container(
                              color: const PdfColor.fromInt(0xffbdbdbd),
                              width: 1,
                              height: 10,
                            ),
                            Text("SiteTag 1\nSiteTag 2\nSiteTag 3 ",
                                style: TextStyle(fontSize: 10, fontBold: ttf)),
                            Container(
                              color: const PdfColor.fromInt(0xffbdbdbd),
                              width: 1,
                              height: 10,
                            ),
                            Text("90% ",
                                style: TextStyle(fontSize: 10, fontBold: ttf)),
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              }),
            ),
          ];
        },
      ),
    );

    return PdfApi.saveDocument(jobcard: "123", pdf: pdf);
  }
}
