import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';

import '../model/pdf_model.dart';

@singleton
class PdfService extends ChangeNotifier {
  PdfHeaderModel _pdfModel = PdfHeaderModel(
      reportName: "",
      companyName: "",
      businessUnit: "",
      deviceImei: "",
      shiftStart: 0,
      shiftEnd: 0,
      shiftEdited: 0,
      minTime: 0);

  Future setModel(
      {required String reportName,
      required String companyName,
      String? businessUnit,
      required String deviceImei,
      required int shiftStart,
      required int shiftEnd,
      required int shiftEdited,
      required int minTime}) async {
    _pdfModel = PdfHeaderModel(
        reportName: reportName,
        companyName: companyName,
        businessUnit: businessUnit,
        deviceImei: deviceImei,
        shiftStart: shiftStart,
        shiftEnd: shiftEnd,
        shiftEdited: shiftEdited,
        minTime: minTime);

    print(_pdfModel);
    notifyListeners();
    return _pdfModel;
  }

  PdfHeaderModel get pdfModel => _pdfModel;
}
